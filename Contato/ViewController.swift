//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos: [Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.txt_nome.text = contato.nome
        cell.txt_email.text = contato.email
        cell.txt_endereco.text = contato.endereco
        cell.txt_telefone.text = contato.numero
        
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome: "Bh Rodriguez Sánchez", numero: "31 99999-9999", email: "bhzao1@gmail.com", endereco: "Rua Do Berimbau, 54"))
        listaDeContatos.append(Contato(nome: "Lucimara Bartolmeu", numero: "31 93333-3333", email: "lucimara2@hotmail.com", endereco: "Rua Copo d'água, 245"))
        listaDeContatos.append(Contato(nome: "Rafael Polo Norte", numero: "31 96666-6666", email: "rafarpgs@gmail.com", endereco: "Rua do Pão, 156"))
        listaDeContatos.append(Contato(nome: "Felipe Fonseca Ferdinando", numero: "31 97777-7777", email: "felipefonsequinha@gmail.com", endereco: "Rua Liliane, 190"))
        // Do any additional setup after loading the view.
    }


}

