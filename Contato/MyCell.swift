//
//  MyCell.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var txt_nome: UILabel!
    @IBOutlet weak var txt_email: UILabel!
    @IBOutlet weak var txt_endereco: UILabel!
    @IBOutlet weak var txt_telefone: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
